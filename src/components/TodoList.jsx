import React, { Component } from 'react';
import TableComponents from './TableComponents'

class TodoList extends Component {
    render() {
        return (
         <div style={{display:"flex", alignItems:"center", flexDirection:"column"}}>
           <table style = {{width:500, border: '1px solid black'}}>
               <thead style={{border: '1px solid black'}}>
                   <th>ID</th>
                   <th>Title</th>
               </thead>
               <tbody>
                   {this.props.data.map((todo, index) =>{
                       return <TableComponents todoInfo={todo} key={index} />
                   })}
               </tbody>
           </table>
        </div>
            
        );
    }
}

export default TodoList;
