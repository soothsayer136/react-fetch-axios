import React, { Component } from 'react';
import './Todo.css'

class TableComponents extends Component {
    render() {
        
        return (
            
                <tr>
                <td style={{border: '1px solid black'}}>{this.props.todoInfo.id}</td>
                <td style={{border: '1px solid black'}}> {this.props.todoInfo.title}</td>
                <td className={this.props.todoInfo.completed === false? "marker-not-complete":"marker_complete"}>{this.props.todoInfo.completed===false? "Not Completed":"Completed"}</td>
                </tr>
            
        );
    }
}

export default TableComponents;
