import React, { Component } from 'react';
import axios from 'axios';
import TodoList from './TodoList'

class FetchHandler extends Component {

    state={
        todos:[]
    }
    componentDidMount() {
        this.getDataFromAPI();
    }


    getDataFromAPI() {
        let self = this;
        let url = "https://jsonplaceholder.typicode.com/todos"
        axios.get(url)
        .then((response) => {
            self.setState({
                todos:response.data
            })
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        })
        .then(function () {
            // always executed
        });
            }

    render() {
        // let todoList = this.state.todos.length >0 && this.state.todos.map((item) =>{
        //     return <TodoList data={item}/>
        // })
        return (
            <div>
                <h1>Example of Fetch API</h1>
                <TodoList data={this.state.todos}/>



            </div>
        );
    }
}

export default FetchHandler;
